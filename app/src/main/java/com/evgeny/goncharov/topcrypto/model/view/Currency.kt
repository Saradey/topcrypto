package com.evgeny.goncharov.topcrypto.model.view


/**
 * Created by Evgeny Goncharov on 2019-07-05.
 * jtgn@yandex.ru
 */

//класс данных для элемента списка
data class Currency(
    val id: String,
    val symbol: String,
    val name: String,
    val image: String,
    val price: Float,
    val marketCap: String,
    val marketCapRank: Int,
    val totalVolume: Float,
    val priceChangePercentage24h: Float,
    val marketCapChangePercentage24h: Float,
    val circulatingSupply: Double,
    val totalSupply: Long,
    val ath: Float,
    val athChangePercentage: Float
)