package com.evgeny.goncharov.topcrypto.model


/**
 * Created by Evgeny Goncharov on 2019-07-05.
 * jtgn@yandex.ru
 */


class GeckoCoinChart (
    var prices: List<List<Float>>
)