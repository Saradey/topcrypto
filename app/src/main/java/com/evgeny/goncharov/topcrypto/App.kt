package com.evgeny.goncharov.topcrypto

import android.app.Application
import com.evgeny.goncharov.topcrypto.di.component.AppComponent
import com.evgeny.goncharov.topcrypto.di.component.DaggerAppComponent
import com.evgeny.goncharov.topcrypto.di.module.AppModule
import com.evgeny.goncharov.topcrypto.di.module.ChartModule
import com.evgeny.goncharov.topcrypto.di.module.MvpModule
import com.evgeny.goncharov.topcrypto.di.module.RestModule


/**
 * Created by Evgeny Goncharov on 2019-07-05.
 * jtgn@yandex.ru
 */


class App : Application(){


    companion object {
        lateinit var appComponent: AppComponent
    }




    override fun onCreate() {
        super.onCreate()

        initializeDagger()
    }


    private fun initializeDagger() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .restModule(RestModule())
            .mvpModule(MvpModule())
            .chartModule(ChartModule())
            .build()
    }

}