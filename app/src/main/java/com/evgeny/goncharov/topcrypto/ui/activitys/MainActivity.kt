package com.evgeny.goncharov.topcrypto.ui.activitys

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import com.evgeny.goncharov.topcrypto.R
import com.evgeny.goncharov.topcrypto.ui.fragments.CurrenciesListFragment
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.InterstitialAd

class MainActivity : AppCompatActivity() {

    //межрастранияный баннер
    private lateinit var mInterAdMov: InterstitialAd

//id приложения
    //ca-app-pub-8929112725463592~7672019073

    //id баннера
    //ca-app-pub-8929112725463592/8434436401

    //id межстраничного
    //ca-app-pub-8929112725463592/2651039955

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //межрастранияный баннер
        mInterAdMov = InterstitialAd(this)
        mInterAdMov.adUnitId = "ca-app-pub-8929112725463592/2651039955"
        mInterAdMov.loadAd(AdRequest.Builder().build())


        //если был переворот, то мы заново не будет создовать фрагмент
        savedInstanceState ?: supportFragmentManager.beginTransaction()
            .add(R.id.container, CurrenciesListFragment(), null)
            .commit()
    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main_appbar, menu)
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.my_action -> {
                startActivity(
                    Intent(
                        this,
                        AboutActivity::class.java
                    )
                )
            }
        }
        return super.onOptionsItemSelected(item)
    }


    //момент показа межстраничного
    private fun showAd() {
        if (mInterAdMov.isLoaded) {
            mInterAdMov.show()
        }
    }


    //отображаем межстраничный баннер при выходе из приложения
    override fun onBackPressed() {
        super.onBackPressed()
        showAd()
    }
}
