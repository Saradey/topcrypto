package com.evgeny.goncharov.topcrypto.ui.activitys

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.evgeny.goncharov.topcrypto.R
import com.google.android.gms.ads.AdRequest
import kotlinx.android.synthetic.main.activity_about.*


/**
 * Created by Evgeny Goncharov on 2019-07-03.
 * jtgn@yandex.ru
 */


class AboutActivity : AppCompatActivity() {





    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about)

        //иницилизация Admob
        val adM = AdRequest.Builder().build()
        adView.loadAd(adM)

        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        buttonRateApp.setOnClickListener {
            //открыть страницу приложения в play market
            startActivity(
                Intent(Intent.ACTION_VIEW,
                Uri.parse("market://details?id=${applicationContext.packageName}"))
            )
        }
    }





}