package com.evgeny.goncharov.topcrypto.mvp.presenters

import com.evgeny.goncharov.topcrypto.App
import com.evgeny.goncharov.topcrypto.aop.formatThousands
import com.evgeny.goncharov.topcrypto.model.view.Currency
import com.evgeny.goncharov.topcrypto.mvp.contracts.CurrenciesContract
import com.evgeny.goncharov.topcrypto.rest.CoinGeckoApi
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*
import javax.inject.Inject


/**
 * Created by Evgeny Goncharov on 2019-07-05.
 * jtgn@yandex.ru
 */


class CurrenciesPresenter : CurrenciesContract.Presenter() {

    @Inject
    lateinit var geckoApi: CoinGeckoApi

    init {
        App.appComponent.inject(this)
    }


    override fun makeList() {
        view.showProgress()

        //подписываемся на поток данных
        subscribe(
            geckoApi.getCoinMarket()
                //определяем отдельный поток для отправки данных
                .subscribeOn(Schedulers.io())
                //получаем данные в основном потоке
                .observeOn(AndroidSchedulers.mainThread())
                //преобразуем List<GeckoCoin> в Observable<GeckoCoin>
                .flatMap { Observable.fromIterable(it) }
                //наполняем поля элемента списка для адаптера
                .doOnNext {
                    view.addCurrency(
                        Currency(
                            it.id,
                            it.symbol,
                            it.name,
                            it.image,
                            it.current_price,
                            it.market_cap.formatThousands(),
                            it.market_cap_rank,
                            it.total_volume,
                            it.price_change_percentage_24h,
                            it.market_cap_change_percentage_24h,
                            it.circulating_supply,
                            it.total_supply,
                            it.ath,
                            it.ath_change_percentage
                        )
                    )
                }
                //вызывается при вызове onComplete
                .doOnComplete {
                    view.hideProgress()
                }
                //подписывает Observer на Observable
                .subscribe({
                    view.hideProgress()
                    //оповещаем адаптер об изменениях списка
                    view.notifyAdapter()
                }, {
                    view.showErrorMessage(it.message)
                    view.hideProgress()
                    it.printStackTrace()
                })
        )
    }


    //обновляем список
    override fun refreshList() {
        view.refresh()
        makeList()
    }


}