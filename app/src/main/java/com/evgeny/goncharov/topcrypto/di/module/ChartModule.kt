package com.evgeny.goncharov.topcrypto.di.module

import com.evgeny.goncharov.topcrypto.design.LatestChart
import com.evgeny.goncharov.topcrypto.utils.YearValueFormatter
import dagger.Module
import dagger.Provides


/**
 * Created by Evgeny Goncharov on 2019-07-05.
 * jtgn@yandex.ru
 */

//для работы с графиком
@Module
class ChartModule {

    @Provides
    fun provideFormatter()
            : YearValueFormatter = YearValueFormatter()

    @Provides
    fun provideLatestChart()
            : LatestChart = LatestChart()

}
