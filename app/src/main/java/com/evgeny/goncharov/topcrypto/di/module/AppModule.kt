package com.evgeny.goncharov.topcrypto.di.module

import android.content.Context
import com.evgeny.goncharov.topcrypto.App
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


/**
 * Created by Evgeny Goncharov on 2019-07-05.
 * jtgn@yandex.ru
 */

@Module
class AppModule(private val app: App) {

    @Provides
    @Singleton
    fun provideContext(): Context = app

}