package com.evgeny.goncharov.topcrypto.di.module

import com.evgeny.goncharov.topcrypto.mvp.presenters.CurrenciesPresenter
import com.evgeny.goncharov.topcrypto.mvp.presenters.LatestChartPresenter
import dagger.Module
import dagger.Provides


/**
 * Created by Evgeny Goncharov on 2019-07-05.
 * jtgn@yandex.ru
 */


@Module
class MvpModule {

    @Provides
    fun provideCurrenciesPresenter()
            : CurrenciesPresenter = CurrenciesPresenter()



    @Provides
    fun provideLatestChartPresenter()
            : LatestChartPresenter = LatestChartPresenter()

}
