package com.evgeny.goncharov.topcrypto.aop

import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by Evgeny Goncharov on 2019-07-05.
 * jtgn@yandex.ru
 */


//использовать в списке криптовалют для разделения порядков в числах
fun Float.formatThousands() : String {
    val sb = StringBuilder()
    val formatter = Formatter(sb, Locale.US)
    formatter.format("%(,.0f", this)
    return sb.toString()
}

//для перевода дат из UNIX в строковый формат для использования на графике.
fun Number.dateToString(pattern: String): String {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this.toLong()
    return SimpleDateFormat(pattern).format(calendar.time)
}