package com.evgeny.goncharov.topcrypto.mvp.contracts

import com.evgeny.goncharov.topcrypto.model.view.Currency
import com.evgeny.goncharov.topcrypto.ui.adapters.CurrenciesAdapter


/**
 * Created by Evgeny Goncharov on 2019-07-05.
 * jtgn@yandex.ru
 */


class CurrenciesContract {
    interface View : BaseContract.View {
        fun addCurrency(currency: Currency)
        fun notifyAdapter()
        fun showProgress()
        fun hideProgress()
        fun showErrorMessage(error: String?)
        fun refresh()
    }

    abstract class Presenter : BaseContract.Presenter<View>() {
        abstract fun makeList()
        abstract fun refreshList()
    }
}