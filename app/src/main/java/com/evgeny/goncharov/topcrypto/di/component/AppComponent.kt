package com.evgeny.goncharov.topcrypto.di.component

import com.evgeny.goncharov.topcrypto.design.LatestChart
import com.evgeny.goncharov.topcrypto.di.module.AppModule
import com.evgeny.goncharov.topcrypto.di.module.ChartModule
import com.evgeny.goncharov.topcrypto.di.module.MvpModule
import com.evgeny.goncharov.topcrypto.di.module.RestModule
import com.evgeny.goncharov.topcrypto.mvp.presenters.CurrenciesPresenter
import com.evgeny.goncharov.topcrypto.mvp.presenters.LatestChartPresenter
import com.evgeny.goncharov.topcrypto.ui.activitys.ChartActivity
import com.evgeny.goncharov.topcrypto.ui.activitys.MainActivity
import com.evgeny.goncharov.topcrypto.ui.fragments.CurrenciesListFragment
import dagger.Component
import javax.inject.Singleton


/**
 * Created by Evgeny Goncharov on 2019-07-05.
 * jtgn@yandex.ru
 */



@Component(modules = arrayOf(AppModule::class, RestModule::class, MvpModule::class, ChartModule::class))
@Singleton
interface AppComponent {

    //activity
    fun inject(mainActivity: MainActivity)
    fun inject(chartActivity: ChartActivity)

    //presenter
    fun inject(presenter: CurrenciesPresenter)
    fun inject(presenter: LatestChartPresenter)

    //fragment
    fun inject(fragment : CurrenciesListFragment)

    //design
    fun inject(chart : LatestChart)
}